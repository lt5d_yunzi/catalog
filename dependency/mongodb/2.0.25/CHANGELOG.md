# Changelog



## [mongodb-2.0.25](https://github.com/truecharts/charts/compare/mongodb-2.0.24...mongodb-2.0.25) (2022-09-30)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3965](https://github.com/truecharts/charts/issues/3965))




## [mongodb-2.0.24](https://github.com/truecharts/charts/compare/mongodb-2.0.23...mongodb-2.0.24) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.5 ([#3946](https://github.com/truecharts/charts/issues/3946))




## [mongodb-2.0.23](https://github.com/truecharts/charts/compare/mongodb-2.0.22...mongodb-2.0.23) (2022-09-28)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [mongodb-2.0.22](https://github.com/truecharts/charts/compare/mongodb-2.0.21...mongodb-2.0.22) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.4 ([#3914](https://github.com/truecharts/charts/issues/3914))




## [mongodb-2.0.21](https://github.com/truecharts/charts/compare/mongodb-2.0.20...mongodb-2.0.21) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.1 ([#3893](https://github.com/truecharts/charts/issues/3893))




## [mongodb-2.0.20](https://github.com/truecharts/charts/compare/mongodb-2.0.19...mongodb-2.0.20) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.0




## [mongodb-2.0.19](https://github.com/truecharts/charts/compare/mongodb-2.0.17...mongodb-2.0.19) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3860](https://github.com/truecharts/charts/issues/3860))
  - update helm chart common to v10.5.12




## [mongodb-2.0.18](https://github.com/truecharts/charts/compare/mongodb-2.0.17...mongodb-2.0.18) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.5.12




## [mongodb-2.0.17](https://github.com/truecharts/charts/compare/mongodb-2.0.13...mongodb-2.0.17) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

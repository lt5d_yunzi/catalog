# Changelog



## [postgresql-8.0.90](https://github.com/truecharts/charts/compare/postgresql-8.0.89...postgresql-8.0.90) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update helm chart common to v10.6.5 ([#3946](https://github.com/truecharts/charts/issues/3946))
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [cryptpad-0.0.62](https://github.com/truecharts/charts/compare/cryptpad-0.0.61...cryptpad-0.0.62) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))
  - update docker general non-major ([#3920](https://github.com/truecharts/charts/issues/3920))




## [cryptpad-0.0.61](https://github.com/truecharts/charts/compare/cryptpad-0.0.60...cryptpad-0.0.61) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [cryptpad-0.0.60](https://github.com/truecharts/charts/compare/cryptpad-0.0.59...cryptpad-0.0.60) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3906](https://github.com/truecharts/charts/issues/3906))




## [cryptpad-0.0.59](https://github.com/truecharts/charts/compare/cryptpad-0.0.58...cryptpad-0.0.59) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [cryptpad-0.0.58](https://github.com/truecharts/charts/compare/cryptpad-0.0.57...cryptpad-0.0.58) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3881](https://github.com/truecharts/charts/issues/3881))




## [cryptpad-0.0.57](https://github.com/truecharts/charts/compare/cryptpad-0.0.56...cryptpad-0.0.57) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3860](https://github.com/truecharts/charts/issues/3860))




## [cryptpad-0.0.56](https://github.com/truecharts/charts/compare/cryptpad-0.0.55...cryptpad-0.0.56) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [cryptpad-0.0.55](https://github.com/truecharts/charts/compare/cryptpad-0.0.50...cryptpad-0.0.55) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

# Changelog



## [promtail-3.0.56](https://github.com/truecharts/charts/compare/promtail-3.0.55...promtail-3.0.56) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.5 ([#3946](https://github.com/truecharts/charts/issues/3946))




## [promtail-3.0.55](https://github.com/truecharts/charts/compare/promtail-3.0.54...promtail-3.0.55) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.4 ([#3914](https://github.com/truecharts/charts/issues/3914))




## [promtail-3.0.54](https://github.com/truecharts/charts/compare/promtail-3.0.53...promtail-3.0.54) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.1 ([#3893](https://github.com/truecharts/charts/issues/3893))




## [promtail-3.0.53](https://github.com/truecharts/charts/compare/promtail-3.0.52...promtail-3.0.53) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.0




## [promtail-3.0.52](https://github.com/truecharts/charts/compare/promtail-3.0.51...promtail-3.0.52) (2022-09-22)

### Chore

- refactor Services SCALE GUI
  - update helm chart common to v10.5.12
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [promtail-3.0.51](https://github.com/truecharts/charts/compare/promtail-3.0.50...promtail-3.0.51) (2022-09-13)

### Chore

- update helm chart common to v10.5.10




## [promtail-3.0.50](https://github.com/truecharts/charts/compare/promtail-3.0.49...promtail-3.0.50) (2022-09-11)

### Chore

- update helm chart common to v10.5.9




## [promtail-3.0.49](https://github.com/truecharts/charts/compare/promtail-3.0.48...promtail-3.0.49) (2022-09-10)

### Chore

- update helm chart common to v10.5.8 ([#3729](https://github.com/truecharts/charts/issues/3729))




## [promtail-3.0.48](https://github.com/truecharts/charts/compare/promtail-3.0.47...promtail-3.0.48) (2022-09-03)

### Fix

- fix serviceaccount creation on few apps ([#3665](https://github.com/truecharts/charts/issues/3665))




## [promtail-3.0.47](https://github.com/truecharts/charts/compare/promtail-3.0.45...promtail-3.0.47) (2022-08-30)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.5.6 ([#3635](https://github.com/truecharts/charts/issues/3635))
  - update helm chart common to v10.5.5


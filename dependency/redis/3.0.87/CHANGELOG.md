# Changelog



## [redis-3.0.87](https://github.com/truecharts/charts/compare/redis-3.0.86...redis-3.0.87) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.5 ([#3946](https://github.com/truecharts/charts/issues/3946))





## [redis-3.0.85](https://github.com/truecharts/charts/compare/redisinsight-0.0.26...redis-3.0.85) (2022-09-26)

### Chore

- update helm chart common to v10.6.4 ([#3914](https://github.com/truecharts/charts/issues/3914))




## [redis-3.0.84](https://github.com/truecharts/charts/compare/redis-3.0.83...redis-3.0.84) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.1 ([#3893](https://github.com/truecharts/charts/issues/3893))




## [redis-3.0.83](https://github.com/truecharts/charts/compare/redis-3.0.82...redis-3.0.83) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3881](https://github.com/truecharts/charts/issues/3881))




## [redis-3.0.82](https://github.com/truecharts/charts/compare/redis-3.0.81...redis-3.0.82) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.0




## [redis-3.0.81](https://github.com/truecharts/charts/compare/tubearchivist-redisjson-0.0.24...redis-3.0.81) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))
  - update helm chart common to v10.5.12




## [redis-3.0.80](https://github.com/truecharts/charts/compare/redis-3.0.79...redis-3.0.80) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.5.12




## [redis-3.0.79](https://github.com/truecharts/charts/compare/redis-3.0.75...redis-3.0.79) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update docker general non-major ([#3827](https://github.com/truecharts/charts/issues/3827))
  - update helm chart common to v10.5.11 ([#3832](https://github.com/truecharts/charts/issues/3832))
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))



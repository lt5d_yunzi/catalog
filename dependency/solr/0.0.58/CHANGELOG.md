# Changelog



## [solr-0.0.58](https://github.com/truecharts/charts/compare/solr-0.0.57...solr-0.0.58) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.5 ([#3946](https://github.com/truecharts/charts/issues/3946))




## [solr-0.0.57](https://github.com/truecharts/charts/compare/solr-0.0.56...solr-0.0.57) (2022-09-28)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [solr-0.0.56](https://github.com/truecharts/charts/compare/solr-0.0.55...solr-0.0.56) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.4 ([#3914](https://github.com/truecharts/charts/issues/3914))




## [solr-0.0.55](https://github.com/truecharts/charts/compare/solr-0.0.54...solr-0.0.55) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.6.1 ([#3893](https://github.com/truecharts/charts/issues/3893))




## [solr-0.0.54](https://github.com/truecharts/charts/compare/solr-0.0.53...solr-0.0.54) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3881](https://github.com/truecharts/charts/issues/3881))




## [solr-0.0.53](https://github.com/truecharts/charts/compare/solr-0.0.51...solr-0.0.53) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update helm chart common to v10.6.0
  - update helm chart common to v10.5.12




## [solr-0.0.52](https://github.com/truecharts/charts/compare/solr-0.0.51...solr-0.0.52) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - update helm chart common to v10.5.12




## [solr-0.0.51](https://github.com/truecharts/charts/compare/solr-0.0.48...solr-0.0.51) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update docker general non-major ([#3839](https://github.com/truecharts/charts/issues/3839))
  - update docker general non-major ([#3787](https://github.com/truecharts/charts/issues/3787))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))



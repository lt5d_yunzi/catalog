# Changelog



## [blocky-1.1.0](https://github.com/truecharts/charts/compare/blocky-1.0.11...blocky-1.1.0) (2022-09-29)

### Feat

- add DoH support on main ingress ([#3959](https://github.com/truecharts/charts/issues/3959))




## [blocky-1.0.10](https://github.com/truecharts/charts/compare/blocky-1.0.9...blocky-1.0.10) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [blocky-1.0.9](https://github.com/truecharts/charts/compare/blocky-1.0.7...blocky-1.0.9) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]




## [blocky-1.0.7](https://github.com/truecharts/charts/compare/blocky-1.0.6...blocky-1.0.7) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

  ### Fix

- fix GUI apiurl and prometheus metrics ([#3908](https://github.com/truecharts/charts/issues/3908))




## [blocky-1.0.6](https://github.com/truecharts/charts/compare/blocky-1.0.5...blocky-1.0.6) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - bump version

  ### Fix

- allow port changes, allow dot, fix prometheus and fix api access ([#3899](https://github.com/truecharts/charts/issues/3899))




## [blocky-1.0.5](https://github.com/truecharts/charts/compare/blocky-1.0.4...blocky-1.0.5) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [blocky-1.0.4](https://github.com/truecharts/charts/compare/blocky-1.0.3...blocky-1.0.4) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]

  ### Fix

- update redis




## [blocky-1.0.3](https://github.com/truecharts/charts/compare/blocky-1.0.2...blocky-1.0.3) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]

  ### Fix

- use new common SA handler




## [blocky-1.0.2](https://github.com/truecharts/charts/compare/blocky-1.0.1...blocky-1.0.2) (2022-09-25)

### Chore

# Changelog



## [chromium-desktop-g3-0.0.30](https://github.com/truecharts/charts/compare/chromium-desktop-g3-0.0.29...chromium-desktop-g3-0.0.30) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [chromium-desktop-g3-0.0.30](https://github.com/truecharts/charts/compare/chromium-desktop-g3-0.0.29...chromium-desktop-g3-0.0.30) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [chromium-desktop-g3-0.0.29](https://github.com/truecharts/charts/compare/chromium-desktop-g3-0.0.28...chromium-desktop-g3-0.0.29) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [chromium-desktop-g3-0.0.28](https://github.com/truecharts/charts/compare/chromium-desktop-g3-0.0.27...chromium-desktop-g3-0.0.28) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [chromium-desktop-g3-0.0.27](https://github.com/truecharts/charts/compare/chromium-desktop-g3-0.0.24...chromium-desktop-g3-0.0.27) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [chromium-desktop-g3-0.0.27](https://github.com/truecharts/charts/compare/chromium-desktop-g3-0.0.24...chromium-desktop-g3-0.0.27) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [chromium-desktop-g3-0.0.27](https://github.com/truecharts/charts/compare/chromium-desktop-g3-0.0.24...chromium-desktop-g3-0.0.27) (2022-09-21)

### Chore

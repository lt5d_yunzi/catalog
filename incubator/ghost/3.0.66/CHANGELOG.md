# Changelog



## [ghost-3.0.66](https://github.com/truecharts/charts/compare/ghost-3.0.65...ghost-3.0.66) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [ghost-3.0.66](https://github.com/truecharts/charts/compare/ghost-3.0.65...ghost-3.0.66) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [ghost-3.0.65](https://github.com/truecharts/charts/compare/ghost-3.0.64...ghost-3.0.65) (2022-09-28)

### Chore

- update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [ghost-3.0.63](https://github.com/truecharts/charts/compare/ghost-3.0.62...ghost-3.0.63) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [ghost-3.0.62](https://github.com/truecharts/charts/compare/ghost-3.0.61...ghost-3.0.62) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [ghost-3.0.61](https://github.com/truecharts/charts/compare/ghost-3.0.59...ghost-3.0.61) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [ghost-3.0.61](https://github.com/truecharts/charts/compare/ghost-3.0.59...ghost-3.0.61) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))

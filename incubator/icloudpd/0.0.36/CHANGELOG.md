# Changelog



## [icloudpd-0.0.36](https://github.com/truecharts/charts/compare/icloudpd-0.0.35...icloudpd-0.0.36) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [icloudpd-0.0.35](https://github.com/truecharts/charts/compare/icloudpd-0.0.34...icloudpd-0.0.35) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - clean up UI and unneeded variables ([#3901](https://github.com/truecharts/charts/issues/3901))




## [icloudpd-0.0.34](https://github.com/truecharts/charts/compare/icloudpd-0.0.33...icloudpd-0.0.34) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [icloudpd-0.0.33](https://github.com/truecharts/charts/compare/icloudpd-0.0.32...icloudpd-0.0.33) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [icloudpd-0.0.32](https://github.com/truecharts/charts/compare/icloudpd-0.0.31...icloudpd-0.0.32) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [icloudpd-0.0.32](https://github.com/truecharts/charts/compare/icloudpd-0.0.31...icloudpd-0.0.32) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [icloudpd-0.0.32](https://github.com/truecharts/charts/compare/icloudpd-0.0.31...icloudpd-0.0.32) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

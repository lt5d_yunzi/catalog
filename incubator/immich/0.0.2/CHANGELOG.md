# Changelog



## [immich-0.0.2](https://github.com/truecharts/charts/compare/immich-0.0.1...immich-0.0.2) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]

  ### Fix

- add hostAliases due to upstream hardcoded urls ([#3956](https://github.com/truecharts/charts/issues/3956))




## [immich-0.0.1]immich-0.0.1 (2022-09-28)

### Feat

- add immich ([#3942](https://github.com/truecharts/charts/issues/3942))

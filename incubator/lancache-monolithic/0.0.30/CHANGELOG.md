# Changelog



## [lancache-monolithic-0.0.30](https://github.com/truecharts/charts/compare/lancache-monolithic-0.0.29...lancache-monolithic-0.0.30) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [lancache-monolithic-0.0.29](https://github.com/truecharts/charts/compare/lancache-monolithic-0.0.28...lancache-monolithic-0.0.29) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]

  ### Feat

- make include replace simpler ([#3904](https://github.com/truecharts/charts/issues/3904))

  ### Fix

- fix port assignments ([#3798](https://github.com/truecharts/charts/issues/3798))




## [lancache-monolithic-0.0.28](https://github.com/truecharts/charts/compare/lancache-monolithic-0.0.27...lancache-monolithic-0.0.28) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [lancache-monolithic-0.0.27](https://github.com/truecharts/charts/compare/lancache-monolithic-0.0.26...lancache-monolithic-0.0.27) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [lancache-monolithic-0.0.27](https://github.com/truecharts/charts/compare/lancache-monolithic-0.0.26...lancache-monolithic-0.0.27) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [lancache-monolithic-0.0.27](https://github.com/truecharts/charts/compare/lancache-monolithic-0.0.26...lancache-monolithic-0.0.27) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

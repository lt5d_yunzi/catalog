# Changelog



## [passwordpusherephemeral-0.0.43](https://github.com/truecharts/charts/compare/passwordpusherephemeral-0.0.42...passwordpusherephemeral-0.0.43) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [passwordpusherephemeral-0.0.43](https://github.com/truecharts/charts/compare/passwordpusherephemeral-0.0.42...passwordpusherephemeral-0.0.43) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [passwordpusherephemeral-0.0.42](https://github.com/truecharts/charts/compare/passwordpusherephemeral-0.0.41...passwordpusherephemeral-0.0.42) (2022-09-28)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))
  - update docker general non-major ([#3920](https://github.com/truecharts/charts/issues/3920))




## [passwordpusherephemeral-0.0.41](https://github.com/truecharts/charts/compare/passwordpusherephemeral-0.0.40...passwordpusherephemeral-0.0.41) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [passwordpusherephemeral-0.0.40](https://github.com/truecharts/charts/compare/passwordpusherephemeral-0.0.39...passwordpusherephemeral-0.0.40) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3906](https://github.com/truecharts/charts/issues/3906))




## [passwordpusherephemeral-0.0.39](https://github.com/truecharts/charts/compare/passwordpusherephemeral-0.0.38...passwordpusherephemeral-0.0.39) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [passwordpusherephemeral-0.0.38](https://github.com/truecharts/charts/compare/passwordpusherephemeral-0.0.37...passwordpusherephemeral-0.0.38) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3881](https://github.com/truecharts/charts/issues/3881))




## [passwordpusherephemeral-0.0.37](https://github.com/truecharts/charts/compare/passwordpusherephemeral-0.0.36...passwordpusherephemeral-0.0.37) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [passwordpusherephemeral-0.0.36](https://github.com/truecharts/charts/compare/passwordpusherephemeral-0.0.33...passwordpusherephemeral-0.0.36) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

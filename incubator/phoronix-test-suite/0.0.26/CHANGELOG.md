# Changelog



## [phoronix-test-suite-0.0.26](https://github.com/truecharts/charts/compare/phoronix-test-suite-0.0.25...phoronix-test-suite-0.0.26) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [phoronix-test-suite-0.0.25](https://github.com/truecharts/charts/compare/phoronix-test-suite-0.0.24...phoronix-test-suite-0.0.25) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [phoronix-test-suite-0.0.24](https://github.com/truecharts/charts/compare/phoronix-test-suite-0.0.23...phoronix-test-suite-0.0.24) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [phoronix-test-suite-0.0.24](https://github.com/truecharts/charts/compare/phoronix-test-suite-0.0.23...phoronix-test-suite-0.0.24) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [phoronix-test-suite-0.0.24](https://github.com/truecharts/charts/compare/phoronix-test-suite-0.0.23...phoronix-test-suite-0.0.24) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [phoronix-test-suite-0.0.24](https://github.com/truecharts/charts/compare/phoronix-test-suite-0.0.23...phoronix-test-suite-0.0.24) (2022-09-20)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

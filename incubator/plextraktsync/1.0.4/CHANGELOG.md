# Changelog



## [plextraktsync-1.0.4](https://github.com/truecharts/charts/compare/plextraktsync-1.0.3...plextraktsync-1.0.4) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [plextraktsync-1.0.3](https://github.com/truecharts/charts/compare/plextraktsync-1.0.2...plextraktsync-1.0.3) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [plextraktsync-1.0.2](https://github.com/truecharts/charts/compare/plextraktsync-0.0.30...plextraktsync-1.0.2) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))

  ### Fix

- cron type does not work ([#3811](https://github.com/truecharts/charts/issues/3811))
  - add docs and persistence ([#3604](https://github.com/truecharts/charts/issues/3604))




## [post-recording-0.0.24](https://github.com/truecharts/charts/compare/post-recording-0.0.23...post-recording-0.0.24) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [post-recording-0.0.24](https://github.com/truecharts/charts/compare/post-recording-0.0.23...post-recording-0.0.24) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [post-recording-0.0.24](https://github.com/truecharts/charts/compare/post-recording-0.0.23...post-recording-0.0.24) (2022-09-20)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

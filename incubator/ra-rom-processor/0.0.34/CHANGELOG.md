# Changelog



## [ra-rom-processor-0.0.34](https://github.com/truecharts/charts/compare/ra-rom-processor-0.0.33...ra-rom-processor-0.0.34) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [ra-rom-processor-0.0.33](https://github.com/truecharts/charts/compare/ra-rom-processor-0.0.32...ra-rom-processor-0.0.33) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [ra-rom-processor-0.0.32](https://github.com/truecharts/charts/compare/ra-rom-processor-0.0.31...ra-rom-processor-0.0.32) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [ra-rom-processor-0.0.31](https://github.com/truecharts/charts/compare/ra-rom-processor-0.0.29...ra-rom-processor-0.0.31) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [ra-rom-processor-0.0.31](https://github.com/truecharts/charts/compare/ra-rom-processor-0.0.29...ra-rom-processor-0.0.31) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [ra-rom-processor-0.0.31](https://github.com/truecharts/charts/compare/ra-rom-processor-0.0.29...ra-rom-processor-0.0.31) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))

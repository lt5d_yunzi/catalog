# Changelog



## [rdesktop-0.0.35](https://github.com/truecharts/charts/compare/rdesktop-0.0.34...rdesktop-0.0.35) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [rdesktop-0.0.34](https://github.com/truecharts/charts/compare/rdesktop-0.0.32...rdesktop-0.0.34) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - remove docker sock from everywhere ([#3910](https://github.com/truecharts/charts/issues/3910))




## [rdesktop-0.0.32](https://github.com/truecharts/charts/compare/rdesktop-0.0.31...rdesktop-0.0.32) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [rdesktop-0.0.31](https://github.com/truecharts/charts/compare/rdesktop-0.0.30...rdesktop-0.0.31) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3881](https://github.com/truecharts/charts/issues/3881))




## [rdesktop-0.0.30](https://github.com/truecharts/charts/compare/rdesktop-0.0.28...rdesktop-0.0.30) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [rdesktop-0.0.30](https://github.com/truecharts/charts/compare/rdesktop-0.0.28...rdesktop-0.0.30) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [rdesktop-0.0.30](https://github.com/truecharts/charts/compare/rdesktop-0.0.28...rdesktop-0.0.30) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

# Changelog



## [rimgo-0.0.68](https://github.com/truecharts/charts/compare/rimgo-0.0.67...rimgo-0.0.68) (2022-09-30)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3965](https://github.com/truecharts/charts/issues/3965))




## [rimgo-0.0.67](https://github.com/truecharts/charts/compare/rimgo-0.0.66...rimgo-0.0.67) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [rimgo-0.0.67](https://github.com/truecharts/charts/compare/rimgo-0.0.66...rimgo-0.0.67) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [rimgo-0.0.66](https://github.com/truecharts/charts/compare/rimgo-0.0.65...rimgo-0.0.66) (2022-09-28)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))
  - update docker general non-major ([#3920](https://github.com/truecharts/charts/issues/3920))




## [rimgo-0.0.65](https://github.com/truecharts/charts/compare/rimgo-0.0.64...rimgo-0.0.65) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [rimgo-0.0.64](https://github.com/truecharts/charts/compare/rimgo-0.0.63...rimgo-0.0.64) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3906](https://github.com/truecharts/charts/issues/3906))




## [rimgo-0.0.63](https://github.com/truecharts/charts/compare/rimgo-0.0.62...rimgo-0.0.63) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [rimgo-0.0.62](https://github.com/truecharts/charts/compare/rimgo-0.0.61...rimgo-0.0.62) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3881](https://github.com/truecharts/charts/issues/3881))




## [rimgo-0.0.61](https://github.com/truecharts/charts/compare/rimgo-0.0.60...rimgo-0.0.61) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3860](https://github.com/truecharts/charts/issues/3860))




## [rimgo-0.0.60](https://github.com/truecharts/charts/compare/rimgo-0.0.59...rimgo-0.0.60) (2022-09-23)

### Chore

# Changelog



## [searxng-0.0.50](https://github.com/truecharts/charts/compare/searxng-0.0.49...searxng-0.0.50) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [searxng-0.0.50](https://github.com/truecharts/charts/compare/searxng-0.0.49...searxng-0.0.50) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [searxng-0.0.49](https://github.com/truecharts/charts/compare/searxng-0.0.48...searxng-0.0.49) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [searxng-0.0.48](https://github.com/truecharts/charts/compare/searxng-0.0.47...searxng-0.0.48) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3906](https://github.com/truecharts/charts/issues/3906))




## [searxng-0.0.47](https://github.com/truecharts/charts/compare/searxng-0.0.46...searxng-0.0.47) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [searxng-0.0.46](https://github.com/truecharts/charts/compare/searxng-0.0.45...searxng-0.0.46) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3860](https://github.com/truecharts/charts/issues/3860))




## [searxng-0.0.45](https://github.com/truecharts/charts/compare/searxng-0.0.44...searxng-0.0.45) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [searxng-0.0.44](https://github.com/truecharts/charts/compare/searxng-0.0.39...searxng-0.0.44) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update docker general non-major ([#3827](https://github.com/truecharts/charts/issues/3827))
  - update docker general non-major ([#3839](https://github.com/truecharts/charts/issues/3839))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))


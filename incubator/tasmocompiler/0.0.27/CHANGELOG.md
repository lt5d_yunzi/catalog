# Changelog



## [tasmocompiler-0.0.27](https://github.com/truecharts/charts/compare/tasmocompiler-0.0.26...tasmocompiler-0.0.27) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [tasmocompiler-0.0.26](https://github.com/truecharts/charts/compare/tasmocompiler-0.0.25...tasmocompiler-0.0.26) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [tasmocompiler-0.0.25](https://github.com/truecharts/charts/compare/tasmocompiler-0.0.23...tasmocompiler-0.0.25) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - add missing descriptions and clean up descriptions for various charts ([#3704](https://github.com/truecharts/charts/issues/3704))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - update helm general non-major ([#3711](https://github.com/truecharts/charts/issues/3711))
  - update docker general non-major ([#3736](https://github.com/truecharts/charts/issues/3736))




## [tasmocompiler-0.0.25](https://github.com/truecharts/charts/compare/tasmocompiler-0.0.23...tasmocompiler-0.0.25) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - add missing descriptions and clean up descriptions for various charts ([#3704](https://github.com/truecharts/charts/issues/3704))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - update helm general non-major ([#3711](https://github.com/truecharts/charts/issues/3711))
  - update docker general non-major ([#3736](https://github.com/truecharts/charts/issues/3736))




## [tasmocompiler-0.0.25](https://github.com/truecharts/charts/compare/tasmocompiler-0.0.23...tasmocompiler-0.0.25) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

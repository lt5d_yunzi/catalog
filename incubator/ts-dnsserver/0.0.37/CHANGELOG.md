# Changelog



## [ts-dnsserver-0.0.37](https://github.com/truecharts/charts/compare/ts-dnsserver-0.0.36...ts-dnsserver-0.0.37) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [ts-dnsserver-0.0.36](https://github.com/truecharts/charts/compare/ts-dnsserver-0.0.35...ts-dnsserver-0.0.36) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [ts-dnsserver-0.0.35](https://github.com/truecharts/charts/compare/ts-dnsserver-0.0.34...ts-dnsserver-0.0.35) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3860](https://github.com/truecharts/charts/issues/3860))




## [ts-dnsserver-0.0.34](https://github.com/truecharts/charts/compare/ts-dnsserver-0.0.32...ts-dnsserver-0.0.34) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [ts-dnsserver-0.0.34](https://github.com/truecharts/charts/compare/ts-dnsserver-0.0.32...ts-dnsserver-0.0.34) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [ts-dnsserver-0.0.34](https://github.com/truecharts/charts/compare/ts-dnsserver-0.0.32...ts-dnsserver-0.0.34) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))

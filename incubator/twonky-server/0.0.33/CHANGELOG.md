# Changelog



## [twonky-server-0.0.33](https://github.com/truecharts/charts/compare/twonky-server-0.0.32...twonky-server-0.0.33) (2022-09-30)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3965](https://github.com/truecharts/charts/issues/3965))




## [twonky-server-0.0.32](https://github.com/truecharts/charts/compare/twonky-server-0.0.31...twonky-server-0.0.32) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [twonky-server-0.0.31](https://github.com/truecharts/charts/compare/twonky-server-0.0.30...twonky-server-0.0.31) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [twonky-server-0.0.30](https://github.com/truecharts/charts/compare/twonky-server-0.0.29...twonky-server-0.0.30) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [twonky-server-0.0.29](https://github.com/truecharts/charts/compare/twonky-server-0.0.27...twonky-server-0.0.29) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3787](https://github.com/truecharts/charts/issues/3787))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [twonky-server-0.0.29](https://github.com/truecharts/charts/compare/twonky-server-0.0.27...twonky-server-0.0.29) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3787](https://github.com/truecharts/charts/issues/3787))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [twonky-server-0.0.29](https://github.com/truecharts/charts/compare/twonky-server-0.0.27...twonky-server-0.0.29) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

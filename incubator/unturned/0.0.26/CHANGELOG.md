# Changelog



## [unturned-0.0.26](https://github.com/truecharts/charts/compare/unturned-0.0.25...unturned-0.0.26) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [unturned-0.0.25](https://github.com/truecharts/charts/compare/unturned-0.0.24...unturned-0.0.25) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [unturned-0.0.24](https://github.com/truecharts/charts/compare/unturned-0.0.23...unturned-0.0.24) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [universal-media-server-0.0.28](https://github.com/truecharts/charts/compare/universal-media-server-0.0.25...universal-media-server-0.0.28) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))

  ### Fix

- fix port and code ([#3829](https://github.com/truecharts/charts/issues/3829))




## [unturned-0.0.24](https://github.com/truecharts/charts/compare/unturned-0.0.23...unturned-0.0.24) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [unturned-0.0.24](https://github.com/truecharts/charts/compare/unturned-0.0.23...unturned-0.0.24) (2022-09-20)

### Chore


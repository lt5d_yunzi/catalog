# Changelog



## [vaulthunters-0.0.30](https://github.com/truecharts/charts/compare/vaulthunters-0.0.29...vaulthunters-0.0.30) (2022-09-30)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3965](https://github.com/truecharts/charts/issues/3965))




## [vaulthunters-0.0.29](https://github.com/truecharts/charts/compare/vaulthunters-0.0.28...vaulthunters-0.0.29) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [vaulthunters-0.0.29](https://github.com/truecharts/charts/compare/vaulthunters-0.0.28...vaulthunters-0.0.29) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [vaulthunters-0.0.28](https://github.com/truecharts/charts/compare/vaulthunters-0.0.27...vaulthunters-0.0.28) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [vaulthunters-0.0.27](https://github.com/truecharts/charts/compare/vaulthunters-0.0.26...vaulthunters-0.0.27) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [vaulthunters-0.0.26](https://github.com/truecharts/charts/compare/vaulthunters-0.0.25...vaulthunters-0.0.26) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [vaulthunters-0.0.26](https://github.com/truecharts/charts/compare/vaulthunters-0.0.25...vaulthunters-0.0.26) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




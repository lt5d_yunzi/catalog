# Changelog



## [vertex-0.0.59](https://github.com/truecharts/charts/compare/vertex-0.0.58...vertex-0.0.59) (2022-09-30)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3965](https://github.com/truecharts/charts/issues/3965))




## [vertex-0.0.58](https://github.com/truecharts/charts/compare/vertex-0.0.57...vertex-0.0.58) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [vertex-0.0.58](https://github.com/truecharts/charts/compare/vertex-0.0.57...vertex-0.0.58) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [vertex-0.0.57](https://github.com/truecharts/charts/compare/vertex-0.0.56...vertex-0.0.57) (2022-09-28)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [vertex-0.0.56](https://github.com/truecharts/charts/compare/vertex-0.0.55...vertex-0.0.56) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [vertex-0.0.55](https://github.com/truecharts/charts/compare/vertex-0.0.54...vertex-0.0.55) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3906](https://github.com/truecharts/charts/issues/3906))




## [vertex-0.0.54](https://github.com/truecharts/charts/compare/vertex-0.0.53...vertex-0.0.54) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [vertex-0.0.53](https://github.com/truecharts/charts/compare/vertex-0.0.52...vertex-0.0.53) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [vertex-0.0.52](https://github.com/truecharts/charts/compare/vertex-0.0.48...vertex-0.0.52) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

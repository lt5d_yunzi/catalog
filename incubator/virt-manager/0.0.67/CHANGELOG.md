# Changelog



## [virt-manager-0.0.67](https://github.com/truecharts/charts/compare/virt-manager-0.0.66...virt-manager-0.0.67) (2022-09-30)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3965](https://github.com/truecharts/charts/issues/3965))




## [virt-manager-0.0.66](https://github.com/truecharts/charts/compare/virt-manager-0.0.65...virt-manager-0.0.66) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [virt-manager-0.0.66](https://github.com/truecharts/charts/compare/virt-manager-0.0.65...virt-manager-0.0.66) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [virt-manager-0.0.65](https://github.com/truecharts/charts/compare/virt-manager-0.0.64...virt-manager-0.0.65) (2022-09-28)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [virt-manager-0.0.64](https://github.com/truecharts/charts/compare/virt-manager-0.0.63...virt-manager-0.0.64) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [virt-manager-0.0.63](https://github.com/truecharts/charts/compare/virt-manager-0.0.62...virt-manager-0.0.63) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3906](https://github.com/truecharts/charts/issues/3906))




## [virt-manager-0.0.62](https://github.com/truecharts/charts/compare/virt-manager-0.0.61...virt-manager-0.0.62) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [virt-manager-0.0.61](https://github.com/truecharts/charts/compare/virt-manager-0.0.60...virt-manager-0.0.61) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3881](https://github.com/truecharts/charts/issues/3881))




## [virt-manager-0.0.60](https://github.com/truecharts/charts/compare/virt-manager-0.0.59...virt-manager-0.0.60) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3860](https://github.com/truecharts/charts/issues/3860))




## [virt-manager-0.0.59](https://github.com/truecharts/charts/compare/virt-manager-0.0.58...virt-manager-0.0.59) (2022-09-23)

### Chore


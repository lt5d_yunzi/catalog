# Changelog



## [alist-2.0.38](https://github.com/truecharts/charts/compare/alist-2.0.37...alist-2.0.38) (2022-09-28)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))
  - update docker general non-major ([#3920](https://github.com/truecharts/charts/issues/3920))




## [alist-2.0.37](https://github.com/truecharts/charts/compare/alist-2.0.36...alist-2.0.37) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [alist-2.0.36](https://github.com/truecharts/charts/compare/alist-2.0.35...alist-2.0.36) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [alist-2.0.35](https://github.com/truecharts/charts/compare/alist-2.0.34...alist-2.0.35) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update container image tccr.io/truecharts/alist to v3.0.1 ([#3861](https://github.com/truecharts/charts/issues/3861))




## [alist-2.0.34](https://github.com/truecharts/charts/compare/alist-2.0.33...alist-2.0.34) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [alist-2.0.34](https://github.com/truecharts/charts/compare/alist-2.0.33...alist-2.0.34) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [alist-2.0.34](https://github.com/truecharts/charts/compare/alist-2.0.33...alist-2.0.34) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

# Changelog



## [budge-2.0.45](https://github.com/truecharts/charts/compare/budge-2.0.44...budge-2.0.45) (2022-09-28)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [budge-2.0.44](https://github.com/truecharts/charts/compare/budge-2.0.43...budge-2.0.44) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [budge-2.0.43](https://github.com/truecharts/charts/compare/budge-2.0.42...budge-2.0.43) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [budge-2.0.42](https://github.com/truecharts/charts/compare/budge-2.0.40...budge-2.0.42) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [budge-2.0.42](https://github.com/truecharts/charts/compare/budge-2.0.40...budge-2.0.42) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [budge-2.0.42](https://github.com/truecharts/charts/compare/budge-2.0.40...budge-2.0.42) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

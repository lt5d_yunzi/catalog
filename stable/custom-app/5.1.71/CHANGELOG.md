# Changelog



## [custom-app-5.1.71](https://github.com/truecharts/charts/compare/custom-app-5.1.70...custom-app-5.1.71) (2022-09-30)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3965](https://github.com/truecharts/charts/issues/3965))




## [custom-app-5.1.70](https://github.com/truecharts/charts/compare/custom-app-5.1.69...custom-app-5.1.70) (2022-09-28)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [custom-app-5.1.69](https://github.com/truecharts/charts/compare/custom-app-5.1.68...custom-app-5.1.69) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [custom-app-5.1.68](https://github.com/truecharts/charts/compare/custom-app-5.1.67...custom-app-5.1.68) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3906](https://github.com/truecharts/charts/issues/3906))




## [custom-app-5.1.67](https://github.com/truecharts/charts/compare/custom-app-5.1.66...custom-app-5.1.67) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [custom-app-5.1.66](https://github.com/truecharts/charts/compare/custom-app-5.1.65...custom-app-5.1.66) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3881](https://github.com/truecharts/charts/issues/3881))




## [custom-app-5.1.65](https://github.com/truecharts/charts/compare/custom-app-5.1.64...custom-app-5.1.65) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [custom-app-5.1.64](https://github.com/truecharts/charts/compare/custom-app-5.1.60...custom-app-5.1.64) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3839](https://github.com/truecharts/charts/issues/3839))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))


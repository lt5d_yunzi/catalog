# Changelog



## [cyberchef-2.0.93](https://github.com/truecharts/charts/compare/cyberchef-2.0.92...cyberchef-2.0.93) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [cyberchef-2.0.93](https://github.com/truecharts/charts/compare/cyberchef-2.0.92...cyberchef-2.0.93) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [cyberchef-2.0.92](https://github.com/truecharts/charts/compare/cyberchef-2.0.91...cyberchef-2.0.92) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [cyberchef-2.0.91](https://github.com/truecharts/charts/compare/cyberchef-2.0.90...cyberchef-2.0.91) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3906](https://github.com/truecharts/charts/issues/3906))




## [cyberchef-2.0.90](https://github.com/truecharts/charts/compare/cyberchef-2.0.89...cyberchef-2.0.90) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [cyberchef-2.0.89](https://github.com/truecharts/charts/compare/cyberchef-2.0.88...cyberchef-2.0.89) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3881](https://github.com/truecharts/charts/issues/3881))




## [cyberchef-2.0.88](https://github.com/truecharts/charts/compare/cyberchef-2.0.87...cyberchef-2.0.88) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3860](https://github.com/truecharts/charts/issues/3860))




## [cyberchef-2.0.87](https://github.com/truecharts/charts/compare/cyberchef-2.0.86...cyberchef-2.0.87) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [cyberchef-2.0.86](https://github.com/truecharts/charts/compare/cyberchef-2.0.82...cyberchef-2.0.86) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

# Changelog



## [docker-compose-2.0.46](https://github.com/truecharts/charts/compare/docker-compose-2.0.45...docker-compose-2.0.46) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [docker-compose-2.0.45](https://github.com/truecharts/charts/compare/docker-compose-2.0.44...docker-compose-2.0.45) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [docker-compose-2.0.44](https://github.com/truecharts/charts/compare/docker-compose-2.0.41...docker-compose-2.0.44) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3787](https://github.com/truecharts/charts/issues/3787))
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3827](https://github.com/truecharts/charts/issues/3827))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [docker-compose-2.0.44](https://github.com/truecharts/charts/compare/docker-compose-2.0.41...docker-compose-2.0.44) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3827](https://github.com/truecharts/charts/issues/3827))
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3787](https://github.com/truecharts/charts/issues/3787))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [docker-compose-2.0.43](https://github.com/truecharts/charts/compare/docker-compose-2.0.41...docker-compose-2.0.43) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3787](https://github.com/truecharts/charts/issues/3787))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [docker-compose-2.0.43](https://github.com/truecharts/charts/compare/docker-compose-2.0.41...docker-compose-2.0.43) (2022-09-20)

### Chore


# Changelog



## [dokuwiki-3.0.57](https://github.com/truecharts/charts/compare/dokuwiki-3.0.56...dokuwiki-3.0.57) (2022-09-28)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [dokuwiki-3.0.56](https://github.com/truecharts/charts/compare/dokuwiki-3.0.55...dokuwiki-3.0.56) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [dokuwiki-3.0.55](https://github.com/truecharts/charts/compare/dokuwiki-3.0.54...dokuwiki-3.0.55) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [dokuwiki-3.0.54](https://github.com/truecharts/charts/compare/dokuwiki-3.0.53...dokuwiki-3.0.54) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3860](https://github.com/truecharts/charts/issues/3860))




## [dokuwiki-3.0.53](https://github.com/truecharts/charts/compare/dokuwiki-3.0.50...dokuwiki-3.0.53) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update container image tccr.io/truecharts/dokuwiki to v20220731.1.0 ([#3791](https://github.com/truecharts/charts/issues/3791))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [dokuwiki-3.0.53](https://github.com/truecharts/charts/compare/dokuwiki-3.0.50...dokuwiki-3.0.53) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update container image tccr.io/truecharts/dokuwiki to v20220731.1.0 ([#3791](https://github.com/truecharts/charts/issues/3791))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [dokuwiki-3.0.53](https://github.com/truecharts/charts/compare/dokuwiki-3.0.50...dokuwiki-3.0.53) (2022-09-21)

### Chore


# Changelog



## [dsmr-reader-8.0.41](https://github.com/truecharts/charts/compare/dsmr-reader-8.0.40...dsmr-reader-8.0.41) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3925](https://github.com/truecharts/charts/issues/3925))




## [dsmr-reader-8.0.40](https://github.com/truecharts/charts/compare/dsmr-reader-8.0.39...dsmr-reader-8.0.40) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))





## [dsmr-reader-8.0.38](https://github.com/truecharts/charts/compare/dsmr-reader-8.0.37...dsmr-reader-8.0.38) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [dsmr-reader-8.0.38](https://github.com/truecharts/charts/compare/dsmr-reader-8.0.37...dsmr-reader-8.0.38) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [dsmr-reader-8.0.38](https://github.com/truecharts/charts/compare/dsmr-reader-8.0.37...dsmr-reader-8.0.38) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [dsmr-reader-8.0.38](https://github.com/truecharts/charts/compare/dsmr-reader-8.0.37...dsmr-reader-8.0.38) (2022-09-20)

### Chore

- Auto-update chart README [skip ci]

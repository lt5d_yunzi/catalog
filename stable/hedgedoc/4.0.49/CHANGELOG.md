# Changelog



## [hedgedoc-4.0.49](https://github.com/truecharts/charts/compare/hedgedoc-4.0.48...hedgedoc-4.0.49) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3925](https://github.com/truecharts/charts/issues/3925))




## [hedgedoc-4.0.48](https://github.com/truecharts/charts/compare/hedgedoc-4.0.47...hedgedoc-4.0.48) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [hedgedoc-4.0.47](https://github.com/truecharts/charts/compare/hedgedoc-4.0.46...hedgedoc-4.0.47) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [hedgedoc-4.0.46](https://github.com/truecharts/charts/compare/hedgedoc-4.0.45...hedgedoc-4.0.46) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [hedgedoc-4.0.46](https://github.com/truecharts/charts/compare/hedgedoc-4.0.45...hedgedoc-4.0.46) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [hedgedoc-4.0.46](https://github.com/truecharts/charts/compare/hedgedoc-4.0.45...hedgedoc-4.0.46) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI

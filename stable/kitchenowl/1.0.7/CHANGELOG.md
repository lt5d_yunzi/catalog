# Changelog



## [kitchenowl-1.0.7](https://github.com/truecharts/charts/compare/kitchenowl-1.0.6...kitchenowl-1.0.7) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [kitchenowl-1.0.6](https://github.com/truecharts/charts/compare/kitchenowl-1.0.5...kitchenowl-1.0.6) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [kitchenowl-1.0.5](https://github.com/truecharts/charts/compare/kitchenowl-1.0.4...kitchenowl-1.0.5) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [kitchenowl-1.0.4](https://github.com/truecharts/charts/compare/kitchenowl-1.0.1...kitchenowl-1.0.4) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3827](https://github.com/truecharts/charts/issues/3827))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [kitchenowl-1.0.4](https://github.com/truecharts/charts/compare/kitchenowl-1.0.1...kitchenowl-1.0.4) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3827](https://github.com/truecharts/charts/issues/3827))
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [kitchenowl-1.0.3](https://github.com/truecharts/charts/compare/kitchenowl-1.0.1...kitchenowl-1.0.3) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

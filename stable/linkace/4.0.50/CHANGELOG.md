# Changelog



## [linkace-4.0.50](https://github.com/truecharts/charts/compare/linkace-4.0.49...linkace-4.0.50) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3925](https://github.com/truecharts/charts/issues/3925))




## [linkace-4.0.49](https://github.com/truecharts/charts/compare/linkace-4.0.48...linkace-4.0.49) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [linkace-4.0.48](https://github.com/truecharts/charts/compare/linkace-4.0.47...linkace-4.0.48) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [linkace-4.0.47](https://github.com/truecharts/charts/compare/linkace-4.0.46...linkace-4.0.47) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]

  ### Fix

- Fixed wrong value for local backups ([#3891](https://github.com/truecharts/charts/issues/3891))




## [linkace-4.0.46](https://github.com/truecharts/charts/compare/linkace-4.0.45...linkace-4.0.46) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [linkace-4.0.46](https://github.com/truecharts/charts/compare/linkace-4.0.45...linkace-4.0.46) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [linkace-4.0.46](https://github.com/truecharts/charts/compare/linkace-4.0.45...linkace-4.0.46) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]

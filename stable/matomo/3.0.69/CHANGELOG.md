# Changelog



## [matomo-3.0.69](https://github.com/truecharts/charts/compare/matomo-3.0.68...matomo-3.0.69) (2022-09-30)

### Chore

- update docker general non-major ([#3965](https://github.com/truecharts/charts/issues/3965))




## [matomo-3.0.67](https://github.com/truecharts/charts/compare/matomo-3.0.66...matomo-3.0.67) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [matomo-3.0.66](https://github.com/truecharts/charts/compare/matomo-3.0.65...matomo-3.0.66) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [matomo-3.0.65](https://github.com/truecharts/charts/compare/matomo-3.0.64...matomo-3.0.65) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [matomo-3.0.64](https://github.com/truecharts/charts/compare/matomo-3.0.61...matomo-3.0.64) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - update helm general non-major ([#3756](https://github.com/truecharts/charts/issues/3756))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [matomo-3.0.64](https://github.com/truecharts/charts/compare/matomo-3.0.61...matomo-3.0.64) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - update helm general non-major ([#3756](https://github.com/truecharts/charts/issues/3756))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [matomo-3.0.64](https://github.com/truecharts/charts/compare/matomo-3.0.61...matomo-3.0.64) (2022-09-21)

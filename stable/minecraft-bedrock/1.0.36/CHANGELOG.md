# Changelog



## [minecraft-bedrock-1.0.36](https://github.com/truecharts/charts/compare/minecraft-bedrock-1.0.35...minecraft-bedrock-1.0.36) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [minecraft-bedrock-1.0.35](https://github.com/truecharts/charts/compare/minecraft-bedrock-1.0.34...minecraft-bedrock-1.0.35) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [minecraft-bedrock-1.0.34](https://github.com/truecharts/charts/compare/minecraft-bedrock-1.0.33...minecraft-bedrock-1.0.34) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]




## [kiwix-serve-1.0.1](https://github.com/truecharts/charts/compare/kiwix-serve-0.0.27...kiwix-serve-1.0.1) (2022-09-23)

### Fix

- Serve all .zim files in the data dir ([#3848](https://github.com/truecharts/charts/issues/3848))




## [kiwix-serve-0.0.27](https://github.com/truecharts/charts/compare/kiwix-serve-0.0.26...kiwix-serve-0.0.27) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [kiwix-serve-0.0.27](https://github.com/truecharts/charts/compare/kiwix-serve-0.0.26...kiwix-serve-0.0.27) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [kiwix-serve-0.0.27](https://github.com/truecharts/charts/compare/kiwix-serve-0.0.26...kiwix-serve-0.0.27) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

# Changelog



## [mkvtoolnix-2.0.35](https://github.com/truecharts/charts/compare/mkvtoolnix-2.0.34...mkvtoolnix-2.0.35) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [mkvtoolnix-2.0.34](https://github.com/truecharts/charts/compare/mkvtoolnix-2.0.33...mkvtoolnix-2.0.34) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [mkvtoolnix-2.0.33](https://github.com/truecharts/charts/compare/mkvtoolnix-2.0.32...mkvtoolnix-2.0.33) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [mkvtoolnix-2.0.33](https://github.com/truecharts/charts/compare/mkvtoolnix-2.0.32...mkvtoolnix-2.0.33) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [mkvtoolnix-2.0.33](https://github.com/truecharts/charts/compare/mkvtoolnix-2.0.32...mkvtoolnix-2.0.33) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [mkvtoolnix-2.0.33](https://github.com/truecharts/charts/compare/mkvtoolnix-2.0.32...mkvtoolnix-2.0.33) (2022-09-20)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

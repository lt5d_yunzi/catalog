# Changelog



## [mosquitto-6.1.3](https://github.com/truecharts/charts/compare/mosquitto-6.1.2...mosquitto-6.1.3) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [mosquitto-6.1.2](https://github.com/truecharts/charts/compare/mosquitto-6.1.1...mosquitto-6.1.2) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]

  ### Fix

- add second ingress question for websockets service ([#3916](https://github.com/truecharts/charts/issues/3916))




## [mosquitto-6.1.1](https://github.com/truecharts/charts/compare/mosquitto-6.1.0...mosquitto-6.1.1) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]

  ### Fix

- use targetPort in mosquitto.conf ([#3911](https://github.com/truecharts/charts/issues/3911))




## [mosquitto-6.1.0](https://github.com/truecharts/charts/compare/mosquitto-6.0.38...mosquitto-6.1.0) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]

  ### Feat

- Add optional websockets support ([#3883](https://github.com/truecharts/charts/issues/3883))




## [mosquitto-6.0.38](https://github.com/truecharts/charts/compare/mosquitto-6.0.37...mosquitto-6.0.38) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [mosquitto-6.0.37](https://github.com/truecharts/charts/compare/mosquitto-6.0.36...mosquitto-6.0.37) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [mosquitto-6.0.37](https://github.com/truecharts/charts/compare/mosquitto-6.0.36...mosquitto-6.0.37) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

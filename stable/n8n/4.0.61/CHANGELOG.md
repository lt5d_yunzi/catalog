# Changelog



## [n8n-4.0.61](https://github.com/truecharts/charts/compare/n8n-4.0.60...n8n-4.0.61) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3925](https://github.com/truecharts/charts/issues/3925))




## [n8n-4.0.60](https://github.com/truecharts/charts/compare/n8n-4.0.59...n8n-4.0.60) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [n8n-4.0.59](https://github.com/truecharts/charts/compare/n8n-4.0.58...n8n-4.0.59) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [n8n-4.0.58](https://github.com/truecharts/charts/compare/n8n-4.0.57...n8n-4.0.58) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [n8n-4.0.57](https://github.com/truecharts/charts/compare/n8n-4.0.54...n8n-4.0.57) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3787](https://github.com/truecharts/charts/issues/3787))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))

  ### Fix

- parse as int when comparing with 0 ([#3824](https://github.com/truecharts/charts/issues/3824))




## [n8n-4.0.57](https://github.com/truecharts/charts/compare/n8n-4.0.54...n8n-4.0.57) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3787](https://github.com/truecharts/charts/issues/3787))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))

  ### Fix

- parse as int when comparing with 0 ([#3824](https://github.com/truecharts/charts/issues/3824))



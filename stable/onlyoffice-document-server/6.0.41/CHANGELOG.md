# Changelog



## [onlyoffice-document-server-6.0.41](https://github.com/truecharts/charts/compare/onlyoffice-document-server-6.0.40...onlyoffice-document-server-6.0.41) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3925](https://github.com/truecharts/charts/issues/3925))




## [onlyoffice-document-server-6.0.40](https://github.com/truecharts/charts/compare/onlyoffice-document-server-6.0.39...onlyoffice-document-server-6.0.40) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [onlyoffice-document-server-6.0.39](https://github.com/truecharts/charts/compare/onlyoffice-document-server-6.0.38...onlyoffice-document-server-6.0.39) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [onlyoffice-document-server-6.0.38](https://github.com/truecharts/charts/compare/onlyoffice-document-server-6.0.37...onlyoffice-document-server-6.0.38) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]




## [onlyoffice-document-server-6.0.37](https://github.com/truecharts/charts/compare/onlyoffice-document-server-6.0.36...onlyoffice-document-server-6.0.37) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [onlyoffice-document-server-6.0.37](https://github.com/truecharts/charts/compare/onlyoffice-document-server-6.0.36...onlyoffice-document-server-6.0.37) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [onlyoffice-document-server-6.0.37](https://github.com/truecharts/charts/compare/onlyoffice-document-server-6.0.36...onlyoffice-document-server-6.0.37) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

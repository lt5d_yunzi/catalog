# Changelog



## [outline-3.0.46](https://github.com/truecharts/charts/compare/outline-3.0.45...outline-3.0.46) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3925](https://github.com/truecharts/charts/issues/3925))




## [outline-3.0.45](https://github.com/truecharts/charts/compare/outline-3.0.44...outline-3.0.45) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [outline-3.0.44](https://github.com/truecharts/charts/compare/outline-3.0.43...outline-3.0.44) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [outline-3.0.43](https://github.com/truecharts/charts/compare/outline-3.0.41...outline-3.0.43) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3839](https://github.com/truecharts/charts/issues/3839))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [outline-3.0.43](https://github.com/truecharts/charts/compare/outline-3.0.41...outline-3.0.43) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3839](https://github.com/truecharts/charts/issues/3839))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [outline-3.0.42](https://github.com/truecharts/charts/compare/outline-3.0.41...outline-3.0.42) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI

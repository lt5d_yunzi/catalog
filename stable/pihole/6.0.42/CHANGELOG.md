# Changelog



## [pihole-6.0.42](https://github.com/truecharts/charts/compare/pihole-6.0.41...pihole-6.0.42) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [pihole-6.0.41](https://github.com/truecharts/charts/compare/pihole-6.0.40...pihole-6.0.41) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [pihole-6.0.40](https://github.com/truecharts/charts/compare/pihole-6.0.38...pihole-6.0.40) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [pihole-6.0.40](https://github.com/truecharts/charts/compare/pihole-6.0.38...pihole-6.0.40) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [pihole-6.0.40](https://github.com/truecharts/charts/compare/pihole-6.0.38...pihole-6.0.40) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))





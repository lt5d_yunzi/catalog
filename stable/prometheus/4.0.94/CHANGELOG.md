# Changelog



## [prometheus-4.0.94](https://github.com/truecharts/charts/compare/prometheus-4.0.93...prometheus-4.0.94) (2022-09-30)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3965](https://github.com/truecharts/charts/issues/3965))




## [prometheus-4.0.93](https://github.com/truecharts/charts/compare/prometheus-4.0.92...prometheus-4.0.93) (2022-09-28)

### Chore

- update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [prometheus-4.0.91](https://github.com/truecharts/charts/compare/prometheus-4.0.90...prometheus-4.0.91) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [prometheus-4.0.90](https://github.com/truecharts/charts/compare/prometheus-4.0.89...prometheus-4.0.90) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [prometheus-4.0.89](https://github.com/truecharts/charts/compare/prometheus-4.0.88...prometheus-4.0.89) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3881](https://github.com/truecharts/charts/issues/3881))




## [prometheus-4.0.88](https://github.com/truecharts/charts/compare/prometheus-4.0.87...prometheus-4.0.88) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3860](https://github.com/truecharts/charts/issues/3860))




## [prometheus-4.0.87](https://github.com/truecharts/charts/compare/prometheus-4.0.83...prometheus-4.0.87) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3827](https://github.com/truecharts/charts/issues/3827))
  - Expose prometheus configuration options in Scale GUI ([#3797](https://github.com/truecharts/charts/issues/3797))




## [prometheus-4.0.87](https://github.com/truecharts/charts/compare/prometheus-4.0.83...prometheus-4.0.87) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

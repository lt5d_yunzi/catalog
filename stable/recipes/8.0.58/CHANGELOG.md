# Changelog



## [recipes-8.0.58](https://github.com/truecharts/charts/compare/recipes-8.0.57...recipes-8.0.58) (2022-09-28)

### Chore

- update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [recipes-8.0.56](https://github.com/truecharts/charts/compare/recipes-8.0.55...recipes-8.0.56) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [recipes-8.0.55](https://github.com/truecharts/charts/compare/recipes-8.0.54...recipes-8.0.55) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [recipes-8.0.54](https://github.com/truecharts/charts/compare/recipes-8.0.52...recipes-8.0.54) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]

  ### Fix

- fix nginx conf ([#3878](https://github.com/truecharts/charts/issues/3878))




## [recipes-8.0.52](https://github.com/truecharts/charts/compare/recipes-8.0.51...recipes-8.0.52) (2022-09-24)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3860](https://github.com/truecharts/charts/issues/3860))




## [recipes-8.0.51](https://github.com/truecharts/charts/compare/recipes-8.0.50...recipes-8.0.51) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [recipes-8.0.50](https://github.com/truecharts/charts/compare/recipes-8.0.49...recipes-8.0.50) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [recipes-8.0.50](https://github.com/truecharts/charts/compare/recipes-8.0.49...recipes-8.0.50) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

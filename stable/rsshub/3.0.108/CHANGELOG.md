# Changelog



## [rsshub-3.0.108](https://github.com/truecharts/charts/compare/rsshub-3.0.107...rsshub-3.0.108) (2022-09-30)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3965](https://github.com/truecharts/charts/issues/3965))




## [rsshub-3.0.107](https://github.com/truecharts/charts/compare/rsshub-3.0.106...rsshub-3.0.107) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [rsshub-3.0.107](https://github.com/truecharts/charts/compare/rsshub-3.0.106...rsshub-3.0.107) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))




## [rsshub-3.0.106](https://github.com/truecharts/charts/compare/rsshub-3.0.105...rsshub-3.0.106) (2022-09-28)

### Chore

- update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [rsshub-3.0.104](https://github.com/truecharts/charts/compare/rsshub-3.0.103...rsshub-3.0.104) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [rsshub-3.0.103](https://github.com/truecharts/charts/compare/rsshub-3.0.102...rsshub-3.0.103) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3906](https://github.com/truecharts/charts/issues/3906))




## [rsshub-3.0.102](https://github.com/truecharts/charts/compare/rsshub-3.0.101...rsshub-3.0.102) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [rsshub-3.0.101](https://github.com/truecharts/charts/compare/rsshub-3.0.100...rsshub-3.0.101) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3881](https://github.com/truecharts/charts/issues/3881))




## [rsshub-3.0.100](https://github.com/truecharts/charts/compare/rsshub-3.0.99...rsshub-3.0.100) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [rsshub-3.0.99](https://github.com/truecharts/charts/compare/rsshub-3.0.94...rsshub-3.0.99) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]

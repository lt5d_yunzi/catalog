# Changelog



## [satisfactory-2.0.38](https://github.com/truecharts/charts/compare/satisfactory-2.0.37...satisfactory-2.0.38) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [satisfactory-2.0.37](https://github.com/truecharts/charts/compare/satisfactory-2.0.36...satisfactory-2.0.37) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3906](https://github.com/truecharts/charts/issues/3906))




## [satisfactory-2.0.36](https://github.com/truecharts/charts/compare/satisfactory-2.0.35...satisfactory-2.0.36) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [satisfactory-2.0.35](https://github.com/truecharts/charts/compare/satisfactory-2.0.34...satisfactory-2.0.35) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [satisfactory-2.0.34](https://github.com/truecharts/charts/compare/satisfactory-2.0.33...satisfactory-2.0.34) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [satisfactory-2.0.34](https://github.com/truecharts/charts/compare/satisfactory-2.0.33...satisfactory-2.0.34) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [satisfactory-2.0.34](https://github.com/truecharts/charts/compare/satisfactory-2.0.33...satisfactory-2.0.34) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

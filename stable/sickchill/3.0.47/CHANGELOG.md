# Changelog



## [sickchill-3.0.47](https://github.com/truecharts/charts/compare/sickchill-3.0.46...sickchill-3.0.47) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))
  - update docker general non-major ([#3920](https://github.com/truecharts/charts/issues/3920))




## [sickchill-3.0.47](https://github.com/truecharts/charts/compare/sickchill-3.0.46...sickchill-3.0.47) (2022-09-29)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3953](https://github.com/truecharts/charts/issues/3953))
  - update docker general non-major ([#3920](https://github.com/truecharts/charts/issues/3920))




## [sickchill-3.0.46](https://github.com/truecharts/charts/compare/sickchill-3.0.45...sickchill-3.0.46) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [sickchill-3.0.45](https://github.com/truecharts/charts/compare/sickchill-3.0.44...sickchill-3.0.45) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [sickchill-3.0.44](https://github.com/truecharts/charts/compare/sickchill-3.0.43...sickchill-3.0.44) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [sickchill-3.0.43](https://github.com/truecharts/charts/compare/sickchill-3.0.41...sickchill-3.0.43) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update docker general non-major ([#3790](https://github.com/truecharts/charts/issues/3790))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [sickchill-3.0.43](https://github.com/truecharts/charts/compare/sickchill-3.0.41...sickchill-3.0.43) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI

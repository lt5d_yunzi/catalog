# Changelog



## [synclounge-2.0.46](https://github.com/truecharts/charts/compare/synclounge-2.0.45...synclounge-2.0.46) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [synclounge-2.0.45](https://github.com/truecharts/charts/compare/synclounge-2.0.44...synclounge-2.0.45) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [synclounge-2.0.44](https://github.com/truecharts/charts/compare/synclounge-2.0.43...synclounge-2.0.44) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [synclounge-2.0.43](https://github.com/truecharts/charts/compare/synclounge-2.0.41...synclounge-2.0.43) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [synclounge-2.0.43](https://github.com/truecharts/charts/compare/synclounge-2.0.41...synclounge-2.0.43) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [synclounge-2.0.43](https://github.com/truecharts/charts/compare/synclounge-2.0.41...synclounge-2.0.43) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3818](https://github.com/truecharts/charts/issues/3818))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))

# Changelog



## [tailscale-1.1.6](https://github.com/truecharts/charts/compare/tailscale-1.1.5...tailscale-1.1.6) (2022-09-30)

### Chore

- Auto-update chart README [skip ci]

  ### Feat

- Enable userspace by default and update docs ([#3967](https://github.com/truecharts/charts/issues/3967))




## [tailscale-1.1.5](https://github.com/truecharts/charts/compare/tailscale-1.1.4...tailscale-1.1.5) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [tailscale-1.1.4](https://github.com/truecharts/charts/compare/tailscale-1.1.3...tailscale-1.1.4) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [tailscale-1.1.3](https://github.com/truecharts/charts/compare/tailscale-1.1.0...tailscale-1.1.3) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3827](https://github.com/truecharts/charts/issues/3827))
  - update docker general non-major ([#3839](https://github.com/truecharts/charts/issues/3839))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [tailscale-1.1.3](https://github.com/truecharts/charts/compare/tailscale-1.1.0...tailscale-1.1.3) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update docker general non-major ([#3839](https://github.com/truecharts/charts/issues/3839))
  - update docker general non-major ([#3827](https://github.com/truecharts/charts/issues/3827))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [tailscale-1.1.1](https://github.com/truecharts/charts/compare/tailscale-1.1.0...tailscale-1.1.1) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

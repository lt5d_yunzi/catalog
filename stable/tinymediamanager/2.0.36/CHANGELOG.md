# Changelog



## [tinymediamanager-2.0.36](https://github.com/truecharts/charts/compare/tinymediamanager-2.0.35...tinymediamanager-2.0.36) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [tinymediamanager-2.0.35](https://github.com/truecharts/charts/compare/tinymediamanager-2.0.34...tinymediamanager-2.0.35) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [tinymediamanager-2.0.34](https://github.com/truecharts/charts/compare/tinymediamanager-2.0.33...tinymediamanager-2.0.34) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [tinymediamanager-2.0.34](https://github.com/truecharts/charts/compare/tinymediamanager-2.0.33...tinymediamanager-2.0.34) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [tinymediamanager-2.0.34](https://github.com/truecharts/charts/compare/tinymediamanager-2.0.33...tinymediamanager-2.0.34) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))




## [tinymediamanager-2.0.34](https://github.com/truecharts/charts/compare/tinymediamanager-2.0.33...tinymediamanager-2.0.34) (2022-09-20)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

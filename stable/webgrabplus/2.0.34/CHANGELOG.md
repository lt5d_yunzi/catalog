# Changelog



## [webgrabplus-2.0.34](https://github.com/truecharts/charts/compare/webgrabplus-2.0.33...webgrabplus-2.0.34) (2022-09-28)

### Chore

- Auto-update chart README [skip ci]
  - update container image tccr.io/truecharts/webgrabplus to v5.0.1 ([#3940](https://github.com/truecharts/charts/issues/3940))




## [webgrabplus-2.0.33](https://github.com/truecharts/charts/compare/webgrabplus-2.0.32...webgrabplus-2.0.33) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [webgrabplus-2.0.32](https://github.com/truecharts/charts/compare/webgrabplus-2.0.31...webgrabplus-2.0.32) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [webgrabplus-2.0.31](https://github.com/truecharts/charts/compare/webgrabplus-2.0.29...webgrabplus-2.0.31) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - update helm general non-major ([#3711](https://github.com/truecharts/charts/issues/3711))




## [webgrabplus-2.0.31](https://github.com/truecharts/charts/compare/webgrabplus-2.0.29...webgrabplus-2.0.31) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - update helm general non-major ([#3711](https://github.com/truecharts/charts/issues/3711))




## [webgrabplus-2.0.31](https://github.com/truecharts/charts/compare/webgrabplus-2.0.29...webgrabplus-2.0.31) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]

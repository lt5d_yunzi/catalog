# Changelog



## [wger-1.1.12](https://github.com/truecharts/charts/compare/wger-1.1.11...wger-1.1.12) (2022-09-30)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3965](https://github.com/truecharts/charts/issues/3965))




## [wger-1.1.11](https://github.com/truecharts/charts/compare/wger-1.1.10...wger-1.1.11) (2022-09-28)

### Chore

- update docker general non-major ([#3938](https://github.com/truecharts/charts/issues/3938))




## [wger-1.1.9](https://github.com/truecharts/charts/compare/wger-1.1.8...wger-1.1.9) (2022-09-27)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3918](https://github.com/truecharts/charts/issues/3918))




## [wger-1.1.8](https://github.com/truecharts/charts/compare/wger-1.1.7...wger-1.1.8) (2022-09-26)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3906](https://github.com/truecharts/charts/issues/3906))




## [wger-1.1.7](https://github.com/truecharts/charts/compare/wger-1.1.6...wger-1.1.7) (2022-09-25)

### Chore

- Auto-update chart README [skip ci]
  - update helm general non-major ([#3898](https://github.com/truecharts/charts/issues/3898))




## [wger-1.1.6](https://github.com/truecharts/charts/compare/wger-1.1.5...wger-1.1.6) (2022-09-23)

### Chore

- Auto-update chart README [skip ci]
  - update docker general non-major ([#3850](https://github.com/truecharts/charts/issues/3850))




## [wger-1.1.5](https://github.com/truecharts/charts/compare/wger-1.1.4...wger-1.1.5) (2022-09-22)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - refactor Services SCALE GUI
  - update docker general non-major ([#3772](https://github.com/truecharts/charts/issues/3772))
  - update helm general non-major ([#3767](https://github.com/truecharts/charts/issues/3767))
  - split serviceSelector ([#3751](https://github.com/truecharts/charts/issues/3751))




## [wger-1.1.5](https://github.com/truecharts/charts/compare/wger-1.1.4...wger-1.1.5) (2022-09-21)

### Chore

- Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
  - Auto-update chart README [skip ci]
